﻿namespace net6ApiUploadAndDownload
{
    public interface IFileService
    {
        void UploadFile(List<IFormFile> files, string subDirectory);
        (string fileType, byte[] archiveData, string archiveName) DownloadFiles(string subDirectory);  //返回3个值
        string SizeConverter(long bytes);
    }

}
